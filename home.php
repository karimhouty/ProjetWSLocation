<?php
    session_start();

    $host     = "localhost";
    $db_name  = "Location";
    $username = "root";
    $password = "root";
    
    try {
            $con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
        } catch(PDOException $exception) {
            $ret.= "Connection error: " . $exception->getMessage();
    }

    $username = $_SESSION['username'];
?>
<DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <title>Projet WS Location</title>
            <link rel="stylesheet" href="style/index.css">
        </head>
        <body>
            <div id="tete">
                <header>
                    <form class="log" method="post" action="logout.php">
                    <h2>Karim HOUTY<br>Master Ingénierie de Conception et de <br>Développement d'Applications</h2>
                    <label>
                        <input type="submit" name="logout" class="button" value="Se deconnecter"/>
                    </label> 
                    </form>      
                </header>
                <nav>
        			<ul class="main_menu">
            		      <li><a href="index.html">Home</a></li>
            		      <li><a href="#" target="affiche">Résérvation</a>
            		      	<ul>
                                    <li><a href="fichier.html" target="affiche">Reserver une Voiture</a></li>
                                    <li><a href="matrice.html" target="affiche">Liste des Reservations</a></li>
                                </ul>
            		      </li>
            		     <li><a href="#" target="affiche">Mes Clients</a>
            		     	<ul>
                                <li><a href="AjouterClient.php" target="affiche">Ajouter un Client</a></li>
                                <li><a href="AfficherClient.php" target="affiche">Liste des Clients</a></li>
                            </ul>
            		     </li>
            				<li><a href="#">Mes Voitures</a>
                                <ul>
                                    <li><a href="AjouterVoiture.php" target="affiche">Ajouter une Voiture</a></li>
                                    <li><a href="AfficherVoiture.php" target="affiche">Liste des Voitures</a></li>
                                </ul>
                            </li>
        				</ul>   
                </nav>
            </div>
            
            <div id="corps">
                <section id="sidebar2">
                    <nav>
                        <ul class="side_menu2">
                            <li><a href="index.html">Home</a></li>
            		      <li><a href="#" target="affiche">Résérvation</a>
            		      	<ul>
                                    <li><a href="fichier.html" target="affiche">Reserver une Voiture</a></li>
                                    <li><a href="matrice.html" target="affiche">Liste des Reservations</a></li>
                                </ul>
            		      </li>
            		     <li><a href="#" target="affiche">Mes Clients</a>
            		     	<ul>
                                <li><a href="AjouterClient.php" target="affiche">Ajouter un Client</a></li>
                                <li><a href="AfficherClient.php" target="affiche">Liste des Clients</a></li>
                            </ul>
            		     </li>
            				<li><a href="#">Mes Voitures</a>
                                <ul>
                                    <li><a href="AjouterVoiture.php" target="affiche">Ajouter une Voiture</a></li>
                                    <li><a href="AfficherVoiture.php" target="affiche">Liste des Voitures</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </section>
                <section id="affiche">
                    <iframe width="839px" height="580px" name="affiche" src="" allowtransparency="true" style="border:2px solid black;" ></iframe>
                </section>
            </div>
                <footer>
                    E-mail: karim.houty@gmail.com<br>
                    Tel   : 06.58.78.06.20
                </footer>
        </body>
    </html>