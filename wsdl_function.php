<?php
	
	require_once "./wsdl.class.php";	
	
	function getVoiture(){
		$ret="";
		$host     = "localhost";
		$db_name  = "Location";
		$username = "root";
		$password = "root";


		try {
			$con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
		} catch(PDOException $exception) {
			$ret.= "Connection error: " . $exception->getMessage();
		}

		$ret.= "<body>";
		$ret.= "<table border='1'>";
		$ret.= "<tr>";
		$ret.= "<th>IdVoiture</th>";
		$ret.= "<th>Marque</th>";
		$ret.= "<th>Modele</th>";
		$ret.= "<th>Matricule</th>";
		$ret.= "<th>Carburant</th>";
		$ret.= "<th>Photo</th>";
		$ret.= "</tr>";

		$reponse = $con->query("SELECT * FROM Voiture");
		
		while($donnees = $reponse->fetch(PDO::FETCH_ASSOC)) {
			extract($donnees);
			$ret.= "<tr>";
			$ret.= "<td>$IdVoiture</td>";
			$ret.= "<td>$Marque</td>";
			$ret.= "<td>$Modele</td>";
			$ret.= "<td>$Matricule</td>";
			$ret.= "<td>$Carburant</td>";
			$ret.= "<td><img src='images/$Photo' width=100cm height=100cm></td>";
			$ret.= "<td><a href='supprimerVoiture.php?code=$IdVoiture'>Supprimer</a></td>";
			$ret.= "<td><a href='modifierVoiture.php?code=$IdVoiture'>Modifier</a></td>";
			$ret.= "</tr>";
			
		}
		$ret.= "</table>";
		$ret.= "</body>";

		$con = null;
		return $ret;
	}

	function getClient(){
		$ret="";
		$host     = "localhost";
		$db_name  = "Location";
		$username = "root";
		$password = "root";


		try {
			$con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
		} catch(PDOException $exception) {
			$ret.= "Connection error: " . $exception->getMessage();
		}

		$ret.= "<body>";
		$ret.= "<table border='1'>";
		$ret.= "<tr>";
		$ret.= "<th>IdClient</th>";
		$ret.= "<th>Nom</th>";
		$ret.= "<th>Prenom</th>";
		$ret.= "<th>CIN</th>";
		$ret.= "<th>Adresse</th>";
		$ret.= "<th>Ville</th>";
		$ret.= "<th>Photo</th>";
		$ret.= "</tr>";

		$reponse = $con->query("SELECT * FROM Client");
		
		while($donnees = $reponse->fetch(PDO::FETCH_ASSOC)) {
			extract($donnees);
			$ret.= "<tr>";
			$ret.= "<td>$IdClient</td>";
			$ret.= "<td>$Nom</td>";
			$ret.= "<td>$Prenom</td>";
			$ret.= "<td>$CIN</td>";
			$ret.= "<td>$Adresse</td>";
			$ret.= "<td>$Ville</td>";
			$ret.= "<td><img src='images/$Photo' width=100cm height=100cm></td>";
			$ret.= "<td><a href='supprimerClient.php?code=$IdClient'>Supprimer</a></td>";
			$ret.= "<td><a href='modifierClient.php?code=$IdClient'>Modifier</a></td>";
			$ret.= "</tr>";
			
		}
		$ret.= "</table>";
		$ret.= "</body>";

		$con = null;
		return $ret;
	}

	/*function getConnexion(){

		session_start();

		$host     = "localhost";
		$db_name  = "Location";
		$username = "root";
		$password = "root";
		$error='';

		try {
			$con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
		} catch(PDOException $exception) {
			$ret.= "Connection error: " . $exception->getMessage();
		}

		if(isset($_POST['login'])){
	
			if (empty($_POST['username']) || empty($_POST['password'])) {
				$error = "Username or Password is invalid";
				header("location: index.php");
			}else{
				$username = $_POST['username'];
				$password = $_POST['password'];

				$reponse = $con->query("SELECT * FROM Employe WHERE Password='$password' AND Username = '$username'");
	
				while($donnees = $reponse->fetch(PDO::FETCH_ASSOC)) {
				extract($donnees);		
				if(($username =='admin')&&($password=='admin')){
					$_SESSION['username']= $username; // Initializing Session
					header("location: home.php"); // Redirecting To Other Page
				}else{
					$_SESSION['username']= $username; // Initializing Session
					header("location: home.php"); // Redirecting To Other Page
					}
				} 
		else {
			$error = "Username or Password is invalid";
			header("location: index.php?notify=Username or Password is invalid");
		}
		
		}
		}
	}*/

	$server = new WSSoap("WsLocation");
	
	$server->register("getVoiture");

	$server->register("getClient");
	
	$server->handle();

?>